#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <sys/wait.h>

enum { BUFFER_SZ = 4096 };

int is_space(char c) {
    return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

char* strspace(char* str) {
    while (*str != '\0' && !is_space(*str)) {
        str++;
    }
    return str;
}

size_t strlen(const char* str) {
    const char* ptr = str;
    while (*ptr != '\0') {
        ptr++;
    }
    return ptr - str;
}

int atoi(const char* str) {
    int num = 0;
    while (*str != '\0') {
        num = num * 10 + (*str - '0');
        str++;
    }
    return num;
}

void die(const char* msg) {
    write(2, msg, strlen(msg));
    _exit(1);
}

void print(int fd, const char* msg) {
    write(fd, msg, strlen(msg));
}

int tcp_listen(int port) {
    int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock < 0) {
        die("Can not open socket\n");
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        die("Can not bind socket\n");
    }

    if (listen(sock, 5) != 0) {
        die("Can not switch socket to listen mode\n");
    }
    return sock;
}

void http_serve(int fd) {
    char buffer[BUFFER_SZ];
    int nread = read(fd, buffer, BUFFER_SZ - 1);
    buffer[nread] = '\0';
    char* begin = strspace(buffer) + 2; /* skip space symbol and '/' */
    char* end = strspace(begin);
    *end = '\0';

    int content_fd = open(begin, O_RDONLY);
    if (content_fd < 0) {
        print(fd, "HTTP/1.1 404 Not Found\r\n\r\n");
        shutdown(fd, SHUT_RDWR);
        close(fd);
        die("Can not open content file\n");
    }

    print(fd, "HTTP/1.1 200 OK\r\n\r\n");
    while ((nread = read(content_fd, buffer, BUFFER_SZ - 1)) > 0) {
        buffer[nread] = '\0';
        write(fd, buffer, strlen(buffer));
    }

    shutdown(fd, SHUT_RDWR);
    close(fd);
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        die("Few arguments\n");
    }

    if (chdir(argv[2]) != 0) {
        die("Can not chdir to root directory\n");
    }

    int port = atoi(argv[1]);
    int fd = tcp_listen(port);

    while (1) {
        int afd = accept(fd, NULL, NULL);
        if (afd < 0) {
            die("Can not accept connection\n");
            continue;
        }

        int pid = fork();
        if (pid == 0) {
            close(fd);
            http_serve(afd);
            _exit(0);
        }

        close(afd);
        while (waitpid(-1, NULL, WNOHANG) > 0) {
        }
    }

    return 0;
}

